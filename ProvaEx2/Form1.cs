﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Reflection;

namespace ProvaEx2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "testo|*.xlsx";
            open.ShowDialog();
            string indirizzo = open.FileName;
            StreamReader read = new StreamReader(indirizzo);
            Excel.Application exAp = new Excel.Application();
            Excel.Workbook exWork;
            Excel.Worksheet exSh;
            
            exWork = exAp.Workbooks.Open( indirizzo, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows,
                                          "\t", false, false, 0, true, 1, 0);
            exSh = (Excel.Worksheet)exWork.Worksheets.get_Item(1);

            Excel.Range range=exSh.UsedRange;
            int rw = range.Rows.Count;
            int cl = range.Columns.Count;
            string str;
            object missing = Missing.Value;
            double num, dob;
            int rCnt;
            int cCnt;
            try
            {
                for (rCnt = 1; rCnt <= rw; rCnt++)
                {
                    for (cCnt = 1; cCnt <= cl;)
                    {
                        range= exSh.Cells[rCnt, cCnt];
                        str =range.Value2;
                        cCnt++;
                        range = exSh.Cells[rCnt, cCnt];
                        num = range.Value2;
                        dob = num * 2;
                        exSh.get_Range("C" + rCnt.ToString(), missing).Value2 = dob.ToString();
                        break;
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().ToString());
            }

            exAp.Visible = true;
           // exWork.Save();
            exWork.Close();
            exAp.Quit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}